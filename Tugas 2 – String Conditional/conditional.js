// ------------------------------------------------------ IF ELSE
console.log("Soal 1 (IF ELSE)");
var nama = "Jane";
var peran = "Werewolf";

if (nama == "" && peran == "") {
  console.log("Nama harus diisi!");
} else if (peran == "") {
  console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
} else if (nama != "" && peran == "Penyihir") {
  console.log("Selamat datang di Dunia Werewolf, " + nama);
  console.log(
    "Halo " +
      peran +
      " " +
      nama +
      " , kamu dapat melihat siapa yang menjadi werewolf!"
  );
} else if (nama != "" && peran == "Guard") {
  console.log("Selamat datang di Dunia Werewolf, " + nama);
  console.log(
    "Halo " +
      peran +
      " " +
      nama +
      " , kamu akan membantu melindungi temanmu dari serangan werewolf."
  );
} else if (nama != "" && peran == "Werewolf") {
  console.log("Selamat datang di Dunia Werewolf, " + nama);
  console.log(
    "Halo " + peran + " " + nama + " , Kamu akan memakan mangsa setiap malam!"
  );
} else {
  console.log(
    "You are no one, Pilih Peran yang tersedia : Werewolf/Guard/Penyihir"
  );
}

// ------------------------------------------------------ Switch Case
// var hari = 21;
// var bulan = 1;
// var tahun = 1945;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945';

console.log("Soal 2 (SWITCH CASE)");

var tanggal = 1; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 12; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2000; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
var bulanDesc;

switch (bulan) {
  case 1: {
    bulanDesc = "Januari";
    break;
  }
  case 2: {
    bulanDesc = "Februari";
    break;
  }
  case 3: {
    bulanDesc = "Maret";
    break;
  }
  case 4: {
    bulanDesc = "April";
    break;
  }
  case 5: {
    bulanDesc = "Mei";
    break;
  }
  case 6: {
    bulanDesc = "Juni";
    break;
  }
  case 7: {
    bulanDesc = "Juli";
    break;
  }
  case 8: {
    bulanDesc = "Agustus";
    break;
  }
  case 9: {
    bulanDesc = "September";
    break;
  }
  case 10: {
    bulanDesc = "Oktober";
    break;
  }
  case 11: {
    bulanDesc = "November";
    break;
  }
  case 12: {
    bulanDesc = "Desember";
    break;
  }
  default: {
    bulanDesc = "Not Defined";
  }
}

console.log(tanggal + " " + bulanDesc + " " + tahun);
