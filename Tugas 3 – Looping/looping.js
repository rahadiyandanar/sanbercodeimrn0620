// ------------------------------------------------------ No. 1 Looping While
var i = 0;
console.log("- Soal 1 (While) :");
console.log("LOOPING PERTAMA");
while (i < 20) {
  i = i + 2;
  console.log(String(i).concat(" - I love coding"));
}
console.log("LOOPING KEDUA");
while (i > 0) {
  console.log(String(i).concat(" - I will become a mobile developer"));
  i = i - 2;
}

// ------------------------------------------------------ No. 2 Looping menggunakan for
console.log("");
console.log("- Soal 2 (For) :");
for (y = 1; y < 21; y++) {
  if (y % 2 == 0) {
    console.log(String(y).concat(" - Berkualitas"));
  } else if (y % 2 != 0 && y % 3 != 0) {
    console.log(String(y).concat(" - Santai"));
  } else if (y % 2 != 0 && y % 3 == 0) {
    console.log(String(y).concat(" - I Love Coding"));
  }
}

// ------------------------------------------------------ No. 3 Membuat Persegi Panjang
console.log("");
console.log("- Soal 3 (Persegi Panjang) :");
var p = 8;
var l = 4;
var row = "";
for (a = 0; a < l; a++) {
  for (b = 0; b < p; b++) {
    row += "#";
  }
  console.log(row);
  row = "";
}

// ------------------------------------------------------ No. 4 Membuat Tangga
console.log("");
console.log("- Soal 4 (Membuat Tangga) :");
var alas = 7;
var row2 = "";
for (c = 0; c < alas; c++) {
  row2 += "#";
  console.log(row2);
}

// ------------------------------------------------------ No. 5 Membuat Papan Catur
console.log("");
console.log("- Soal 5 (Membuat Papan Catur) :");
var p2 = 8;
var l2 = 8;
var row3 = "";
var row4 = "";

for (d = 0; d < p2 / 2; d++) {
  row3 += " #";
}
for (e = 0; e < p2 / 2; e++) {
  row4 += "# ";
}
for (f = 0; f < l2 / 2; f++) {
  console.log(row3);
  console.log(row4);
}
