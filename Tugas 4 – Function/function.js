// ------------------------------------------------------ No. 1
console.log("- Soal 1 :");

teriak = () => {
  return "Halo Sanbers!";
};

console.log(teriak()); // "Halo Sanbers!"

// ------------------------------------------------------ No. 2
console.log("");
console.log("- Soal 2 :");

var num1 = 12;
var num2 = 4;
kalikan = (n1, n2) => {
  return n1 * n2;
};

var hasilKali = kalikan(num1, num2);
console.log(hasilKali); // 30

// ------------------------------------------------------ No. 2
console.log("");
console.log("- Soal 3 :");

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";
introduce = (n, ag, ad, h) => {
  return `Nama Saya ${n}, umur saya ${ag} tahun, alamat saya di ${ad}, dan saya punya hobby yaitu ${hobby}!`;
};

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan); // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!"
