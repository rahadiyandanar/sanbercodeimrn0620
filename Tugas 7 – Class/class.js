class Animal {
  constructor(name, legs, cold_blooded) {
    this.name = name;
    this.legs = 4;
    this.cold_blooded = false;
  }
}

var sheep = new Animal("shaun");

console.log("Soal 1 - Release 1");
console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false

class Ape extends Animal {
  constructor(name) {
    super(name);
  }
  yell() {
    console.log("Auooo");
  }
}

class Frog extends Animal {
  constructor(name) {
    super(name);
  }
  jump() {
    console.log("hop hop");
  }
}

console.log("");
console.log("Soal 1 - Release 2");
var sungokong = new Ape("kera sakti");
console.log(sungokong.name);
sungokong.yell(); // "Auooo"

var kodok = new Frog("buduk");
console.log(kodok.name);
kodok.jump(); // "hop hop"

// ----------------------------------------------------------------------
console.log("");
console.log("Soal 2 - Function to Class");

class Clock {
  constructor({ template }) {
    this.template = template;
  }

  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = "0" + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = "0" + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = "0" + secs;

    var output = this.template
      .replace("h", hours)
      .replace("m", mins)
      .replace("s", secs);

    console.log(output);
  }

  stop() {
    clearInterval(this.timer);
  }

  start() {
    this.render();
    this.timer = setInterval(() => this.render(), 1000);
  }
}

var clock = new Clock({ template: "h:m:s" });
clock.start();
