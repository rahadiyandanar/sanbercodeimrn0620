// ------------------------------------------------------ No. 1
console.log("- Soal 1 (Range) :");

var a1;
range = (a, b) => {
  if (a === undefined || b === undefined) {
    return -1;
  } else if (a > b) {
    a1 = [];
    for (i = a; i >= b; i--) {
      a1.push(i);
    }
    return a1;
  } else if (a < b) {
    a1 = [];
    for (i = a; i <= b; i++) {
      a1.push(i);
    }
    return a1;
  } else {
    ("undefined");
  }
};

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1

// ------------------------------------------------------ No. 2
console.log("");
console.log("- Soal 2 (Range with Step) :");

var a2;
rangeWithStep = (c, d, step) => {
  if (c === undefined || d === undefined || step === undefined) {
    return -1;
  } else if (c > d) {
    a2 = [];
    for (i = c; i >= d; ) {
      a2.push(i);
      i -= step;
    }
    return a2;
  } else if (c < d) {
    a2 = [];
    for (i = c; i <= d; ) {
      a2.push(i);
      i += step;
    }
    return a2;
  } else {
    ("undefined");
  }
};
console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]

// ------------------------------------------------------ No. 3
console.log("");
console.log("- Soal 3 (Sum of Range) :");

var a3;
var a4;

sum = (e, f, g) => {
  if (e === undefined) {
    return 0;
  } else if (e !== undefined && f === undefined) {
    return e;
  } else if (g === undefined) {
    a3 = range(e, f);
  } else if (g !== undefined) {
    a3 = rangeWithStep(e, f, g);
  }
  a4 = 0;
  for (i = 0; i < a3.length; i++) {
    a4 += a3[i];
  }
  return a4;
};

console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0

// ------------------------------------------------------ No. 4
console.log("");
console.log("- Soal 4 (Array Multidimensi) :");

var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];

dataHandling = (arr) => {
  for (i = 0; i < arr.length; i++) {
    console.log("");
    console.log(`Nomor ID : ${arr[i][0]}`);
    console.log(`Nama Lengkap : ${arr[i][1]}`);
    console.log(`TTL : ${arr[i][2]} ${arr[i][3]}`);
    console.log(`Hobi : ${arr[i][4]}`);
  }
};

dataHandling(input);

// ------------------------------------------------------ No. 5
console.log("");
console.log("- Soal 5 (Balik Kata) :");

var y;
var kataBalik;
balikKata = (kata) => {
  y = kata.length;
  kataBalik = "";
  for (i = 0; i < kata.length; i++) {
    y--;
    kataBalik += kata[y];
  }
  return kataBalik;
};

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I

// ------------------------------------------------------ No. 6
console.log("");
console.log("- Soal 6 (Metode Array) :");

var input2 = [
  "0001",
  "Roman Alamsyah ",
  "Bandar Lampung",
  "21/05/1989",
  "Membaca",
];

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */

dataHandling2 = (arr2) => {
  var arr3 = [].concat(arr2); //MUTABLE JS
  arr2.splice(1, 1, "Roman Alamsyah Elsharawy");
  arr2.splice(2, 1, "Provinsi Bandar Lampung");
  arr2.pop();
  arr2.push("Pria");
  arr2.push("SMA Internasional Metro");
  console.log(arr2);

  sMonth1 = arr2[3].split("/");
  switch (parseInt(sMonth1[1])) {
    case 1: {
      console.log("Januari");
      break;
    }
    case 2: {
      console.log("Februari");
      break;
    }
    case 3: {
      console.log("Maret");
      break;
    }
    case 4: {
      console.log("April");
      break;
    }
    case 5: {
      console.log("Mei");
      break;
    }
    case 6: {
      console.log("Juni");
      break;
    }
    case 7: {
      console.log("Juli");
      break;
    }
    case 8: {
      console.log("Agustus");
      break;
    }
    case 9: {
      console.log("September");
      break;
    }
    case 10: {
      console.log("Oktober");
      break;
    }
    case 11: {
      console.log("November");
      break;
    }
    case 12: {
      console.log("Desember");
      break;
    }
    default: {
      console.log("Not Defined");
    }
  }

  var sMonth2 = [].concat(sMonth1); //MUTABLE JS
  console.log(
    sMonth2.sort(function (value1, value2) {
      return value2 - value1;
    })
  );
  console.log(sMonth1.join("-"));

  console.log(arr3[1]); //MUTABLE JS
};

dataHandling2(input2);
