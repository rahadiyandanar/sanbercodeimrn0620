// di index.js
var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

// Tulis code untuk memanggil function readBooks di sini

let iBooksReaded = 0;
let iTime = 10000;
startRead = (iBooksReaded) => {
  if (iBooksReaded < books.length) {
    readBooks(iTime, books[iBooksReaded], function (iTimeSisa) {
      iTime = iTimeSisa;
      iBooksReaded++;
      startRead(iBooksReaded);
    });
  }
};

startRead(0);
setTimeout(function () {
  console.log("Time Up");
}, iTime);
