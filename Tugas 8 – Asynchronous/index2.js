var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

let iBooksReaded = 0;
let iTime = 10000;
startRead = (iBooksReaded) => {
  if (iBooksReaded < books.length) {
    readBooksPromise(iTime, books[iBooksReaded])
      .then((iTimeSisa) => {
        iTime = iTimeSisa;
        iBooksReaded++;
        startRead(iBooksReaded);
      })
      .catch((iTimeSisa) => {
        console.log("Waktu membaca habis, waktu kurang : " + iTimeSisa);
      });
  }
};

startRead(0);
setTimeout(function () {
  console.log("Time Up");
}, iTime);
