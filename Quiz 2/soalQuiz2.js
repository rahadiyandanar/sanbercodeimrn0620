// ==========================================
// 1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
// ==========================================
class Score {
  constructor(subject, points, email) {
    this.subject = subject;
    this.points = points;
    this.email = email;
  }
  average() {
    let avg = 0;
    if (this.points.length > 0) {
      for (let i = 0; i < this.points.length; i++) {
        avg += this.points[i];
      }
      return avg / this.points.length;
    } else {
      return this.points;
    }
  }
}

let oRata = new Score("No-1", [1, 2, 3, 4, 5, 6], "rahadiyandanar@gmail.com");
console.log(`${oRata.subject} ${oRata.points} ${oRata.email}`);
console.log(oRata.average());
console.log("");

// ===========================================
// 2. SOAL Create Score (10 Poin + 5 Poin ES6)
// ===========================================
const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93],
];

viewScores = (data, subject) => {
  let oObject;
  let aNewArray = [];
  let aScore = [];
  //get first array and remove space
  for (let x = 0; x < data[0].length; x++) {
    aNewArray.push(data[0][x].replace(/\s/g, ""));
  }
  //build object
  for (let i = 1; i < data.length; i++) {
    oObject = {
      email: data[i][0],
      subject: subject,
      points: data[i][aNewArray.indexOf(subject)],
    };
    aScore.push(oObject);
  }
  console.log(aScore);
};

// TEST CASE
viewScores(data, "quiz-1");
viewScores(data, "quiz-2");
viewScores(data, "quiz-3");
console.log("");

// ==========================================
// 3. SOAL Recap Score (15 Poin + 5 Poin ES6)
// ==========================================

recapScores = (data) => {
  let oObject2;
  let oRata2;
  let aNilai; //copy
  for (let i = 1; i < data.length; i++) {
    aNilai = [].concat(data[i]);
    aNilai.shift();
    oRata2 = new Score("Rerata", aNilai, data[i][0]);
    oObject2 = {
      Email: oRata2.email,
      Rata: oRata2.average(),
    };
    if (oObject2.Rata >= 90) {
      oObject2.Predikat = "honour";
    } else if (oObject2.Rata >= 80 && oObject2.Rata < 90) {
      oObject2.Predikat = "graduate";
    } else {
      oObject2.Predikat = "participant";
    }

    let { Email, Rata, Predikat } = oObject2;

    console.log(`${i}. Email : ${Email}`);
    console.log(`Rata-rata : ${Rata}`);
    console.log(`Predikat : ${Predikat}`);
    console.log("");
  }
};

recapScores(data);
