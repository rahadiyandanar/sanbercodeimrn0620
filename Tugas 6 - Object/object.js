// ------------------------------------------------------ No. 1
console.log("- Soal 1 (Array to Object) :");
let oObject,
  now = new Date();
const thisYear = now.getFullYear();

function arrayToObject(arr) {
  let aLength = arr.length;
  if (aLength != 0) {
    for (let i = 0; i < aLength; i++) {
      oObject = {
        firstName: arr[i][0],
        lastName: arr[i][1],
        gender: arr[i][2],
        age: arr[i][3],
      };
      if (oObject.age === undefined || thisYear - oObject.age < 0) {
        oObject.age = "Invalid Birth Year";
      } else {
        oObject.age = thisYear - oObject.age;
      }
      console.log(
        `${i + 1}. ${oObject.firstName} ${oObject.lastName} : ${JSON.stringify(
          oObject
        )}`
      );
    }
  } else {
    console.log('""');
  }
}

// Driver Code
const people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);

const people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2020],
];
arrayToObject(people2);

// // Error case
arrayToObject([]); // ""

// ------------------------------------------------------ No. 2
console.log("");
console.log("- Soal 2 (Shopping Time) :");

let listPurchased = [];

const oSale = [
  {
    Product: "Sepatu brand Stacattu",
    Price: 1500000,
  },
  {
    Product: "Baju brand Zoro",
    Price: 500000,
  },
  {
    Product: "Baju brand H&N",
    Price: 250000,
  },
  {
    Product: "Sweater brand Uniklooh",
    Price: 175000,
  },
  {
    Product: "Casing Handphone",
    Price: 50000,
  },
];

shoppingTime = (memberId, money) => {
  if (!memberId) {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } else if (memberId && money < 50000) {
    return "Mohon maaf, uang tidak cukup";
  } else if (memberId && money >= 50000) {
    listPurchased = [];
    moneyTemp = money;
    for (i = 0; i < oSale.length; i++) {
      if (moneyTemp - oSale[i].Price > 0) {
        listPurchased.push(oSale[i].Product);
        moneyTemp -= oSale[i].Price;
      }
    }
    return {
      memberId: memberId,
      money: money,
      listPurchased: listPurchased,
      changeMoney: moneyTemp,
    };
  }
};

console.log(shoppingTime("324193hDew1", 1800000));
console.log(shoppingTime("324193hDew2", 700000));

// ------------------------------------------------------ No. 3
console.log("");
console.log("- Soal 3 (Naik Angkot) :");

function naikAngkot(arrPenumpang) {
  let aJarak;
  let iPrice;
  let aReport = [];
  const iOneRoutePrice = 2000;
  const rute = ["A", "B", "C", "D", "E", "F"];
  for (x = 0; x < arrPenumpang.length; x++) {
    aJarak = [];
    for (i = 0; i < rute.length; i++) {
      //   console.log(arrPenumpang[x][1]);
      if (rute[i] == arrPenumpang[x][1] || rute[i] == arrPenumpang[x][2]) {
        aJarak.push(parseInt(i));
      }
    }
    //Price
    aJarak[0] < aJarak[1]
      ? (iPrice = (aJarak[1] - aJarak[0]) * iOneRoutePrice)
      : (iPrice = aJarak[0] - aJarak[1] * iOneRoutePrice);
    //Object
    aReport.push({
      penumpang: arrPenumpang[x][0],
      naikDari: arrPenumpang[x][1],
      tujuan: arrPenumpang[x][2],
      bayar: iPrice,
    });
  }
  return aReport;
}

//TEST CASE
console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
